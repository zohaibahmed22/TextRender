import {
    CHAPTERFETCH
} from '../actions/Types';

const INITIAL_STATE = {
    data:''
}

export default (state=INITIAL_STATE, action) =>{
    switch(action.type){
        case CHAPTERFETCH:
            return {...state, data: action.payload};
            break;
         default:
            return state;

    }
}