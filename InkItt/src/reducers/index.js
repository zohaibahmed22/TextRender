import { combineReducers } from 'redux';
import NavigationReducers from './NavigationReducers';
import ChapterFetchReducers from './ChapterFetchReducers';

export default combineReducers({
    Navigation: NavigationReducers,
    ChapterFetch: ChapterFetchReducers
});