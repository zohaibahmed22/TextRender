import React, {Component} from 'react';
import { Scene, Router } from 'react-native-router-flux';

import HomePage from './components/HomePage';
import ChapterOne from './components/ChapterOne';
import Chapter from './components/Chapter';
import Book from './components/Book';
import AwesomeBook from './components/AwesomeBook';

class RouterComponent extends Component{

    render(){
        return(
            <Router>
                <Scene>
                    <Scene key="homePage" component={HomePage} title="Home Page" />
                    <Scene key="chapterOne" component={ChapterOne} title="Chapter One" />
                    <Scene key="chapter" component={Chapter} title="Chapter" />
                    <Scene key="awesomeBook" component={AwesomeBook} title="Awesome Book" />
                    <Scene key="book" component={Book} title="Book" />
                </Scene>
            </Router>
        );
    }
}
export default RouterComponent;