import React, {Component} from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { chapterOneClick, chapter, fetchChapterOne } from '../actions';
import { Card,CardSection ,Button} from './common';

let data = 'Loading....';
let str = [];
let testArray = [];

class AwesomeBook extends Component{

    componentWillMount(){
        this.props.fetchChapterOne();
        data = this.props.data.response.text;
       
    }
    onChapterOnePress(){
       
        Actions.chapterOne({data});
    }

    onChapterPress(){
        this.props.chapter();
    }
    render(){
        //this.funct(data);
        return(
            <View>
                <ScrollView>
                <Card>
                    <View style={{backgroundColor: 'white'}}>
                        <Text style={styles.headingStyle}>
                           An Awesome Book
                        </Text>
                    </View>
                    
                </Card>
                <Card>
                    <CardSection>
                        <Button onPress={this.onChapterOnePress.bind(this)}>
                            Chapter One
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onChapterPress.bind(this)}>
                            Chapter Two
                        </Button>
                    </CardSection>
                    <CardSection>
                         <Button onPress={this.onChapterPress.bind(this)}>
                            Chapter Three
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onChapterPress.bind(this)}>
                            Chapter Four
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onChapterPress.bind(this)}>
                            Chapter Five
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onChapterPress.bind(this)}>
                            Chapter Six
                        </Button>
                    </CardSection>
                    <CardSection>
                       <Button onPress={this.onChapterPress.bind(this)}>
                            Chapter Seven
                        </Button>
                    </CardSection>
                </Card>
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    headingStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 30,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    }
};
const mapStateToProps = (state) => {
    const { data } = state.ChapterFetch;
    console.log(data.response);
    return { data };
};

export default connect(mapStateToProps,{chapterOneClick, chapter, fetchChapterOne })(AwesomeBook);