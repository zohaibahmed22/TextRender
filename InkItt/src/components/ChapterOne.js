import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';
import { Card, CardSection, Spinner} from './common';
import { fetchChapterOne } from '../actions';

let data = 'Loading....';
let removedTagArray = [];
let testArray = [];

class ChapterOne extends Component {
    componentWillMount(){
        data = this.props.data;
        this.ReplaceTags(data);
    }

    ReplaceTags (data){
        data = data.replace(' class=""','');

        data = data.replace(/<br>/g,'\n');
        
        data = data.replace(/<p>/g,'\n');
        data = data.replace(/<\/p>/g,'\n')
        
        data = data.replace(/<i>/g,'  ');
        data = data.replace(/<\/i>/g,'  ');
        
        data = data.replace(/<b>/g,'  ');
        data = data.replace(/<\/b>/g,'  ');
        
        removedTagArray = data;
        this.functTest();
    }

    functTest(){
        let italicStr = [];
        let boldStr = [];
        let italicBold = [];
        let regularStr = [];
        let count = 0;
        for(count ;count < removedTagArray.length; count++){
        
            if(removedTagArray[count] === '<'){
                count++;
                console.log('hi from < if');
                if(removedTagArray[count] === '{'){
                    console.log('hi from < { if');
                    count ++;
                    while(removedTagArray[count] !== '}' ){
                        console.log(' hi from  < { while ');
                        italicBold.push(removedTagArray[count]);
                        count++;
                    }
                    testArray.push(<Text key={count} style={{fontStyle:'italic',fontWeight: 'bold'}}>{italicBold}</Text>);
                    italicBold = [];
                }
                   else {
                    console.log(' hi from < else');
                    while(removedTagArray[count] !== '>' ){
                        console.log(' hi from > while ');
                        italicStr.push(removedTagArray[count]);
                        count++;
                    }
                     testArray.push(<Text key= {count} style={{fontStyle: 'italic'}}> {italicStr} </Text>);
                     italicStr = []; 
                }
               }
             //else
                 if(removedTagArray[count] === '{'){
               count ++;
               if(removedTagArray[count] === '<'){
                   while(removedTagArray[count] !== '>'){
                    italicBold.push(removedTagArray[count]);
                    count++;
                }
                testArray.push(<Text key={count} style={{fontStyle:'italic',fontWeight: 'bold'}}>{italicBold}</Text>);
                italicBold = [];
               }else{

                while(removedTagArray[count] !== '}' ){
                    boldStr.push(removedTagArray[count]);
                    count++;
                }
                testArray.push(<Text key={count} style={{fontWeight: 'bold'}}> {boldStr} </Text>);
                boldStr = [];
            }
        }
    //     else {
    //           while(removedTagArray[count+1] !== '<' || removedTagArray[count+1] !== '{'){
    //             regularStr.push(removedTagArray[count]);
    //             count++;
    //           }
    //     testArray.push(<Text key={count}>{regularStr}</Text>)
    //    }
           
            
        }
    }
    render(){
        return(
            <ScrollView style={{margin: '3%'}}>
                <Text>
                    {removedTagArray}
                </Text>
            </ScrollView>
        );
    }
}

export default ChapterOne;