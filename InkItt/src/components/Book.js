import React, {Component} from 'react';
import { View, Text} from 'react-native';
import { Card, CardSection} from './common';
class Book extends Component{

    render(){
        return(
            <View>
                <Card>
                    <CardSection>
                        <Text style={styles.textStyle}>
                            Book not released yet
                        </Text>
                    </CardSection>
                </Card>
            </View>
        );
    }
}
const styles = {
    textStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 30,
        paddingTop: 250,
        paddingBottom: 250
    }
};
export default Book;