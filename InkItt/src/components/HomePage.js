import React, {Component} from 'react';
import { View, Text, ScrollView, NetInfo,ToastAndroid,Platform,Alert } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { chapterOneClick, chapter, fetchChapterOne } from '../actions';
import { Card,CardSection ,Button} from './common';

class HomePage extends Component{

    state = {
        text: '',
    }
    componentWillMount(){
        this.props.fetchChapterOne();
    }
    onAwesomeBookPress(){
        const data = this.props.data;
       console.log(data);
        const response = data.response;
        Actions.awesomeBook({response});
    }
    onBookPress(){
        Actions.book();
    }
    componentDidMount(){
        NetInfo.isConnected.fetch().then(isConnected => {
          this.setState({ text: (isConnected ? 'online' : 'offline')});
          if(Platform.OS === 'android'){
            if(this.state.text === 'online'){
                ToastAndroid.show('Connected to internet',ToastAndroid.SHORT,ToastAndroid.CENTER)
            }else if(this.state.text === 'offline'){
                ToastAndroid.show('please turn on the wifi or mobile data',ToastAndroid.SHORT,ToastAndroid.CENTER)
            }
            else{
                ToastAndroid.show('Checking network connection',ToastAndroid.SHORT,ToastAndroid.CENTER)
            }
         }
         if(Platform.OS === 'ios'){
            Alert.alert(
                'Welcome',
                'Checking Network Connection',
                [
                  { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            )}
        });
        
    }
    componentWillMountAgain(){
        NetInfo.isConnected.fetch().then(isConnected => {
            this.setState({ text: (isConnected ? 'online' : 'offline')});
          });
          if(this.state.text === 'online'){
              ToastAndroid.showWithGravity('Connected to internet',ToastAndroid.SHORT,ToastAndroid.CENTER)
          }else if(this.state.text === 'offline'){
              ToastAndroid.showWithGravity('please turn on the wifi or mobile data',ToastAndroid.SHORT,ToastAndroid.CENTER)
          }
          else{
              ToastAndroid.showWithGravity('Checking network connection',ToastAndroid.SHORT,ToastAndroid.CENTER)
          }
    }
    render(){
        return(
            <View>
                <ScrollView>
                <Card>
                    <View style={{backgroundColor: 'white'}}>
                        <Text style={styles.headingStyle}>
                           Select Book
                        </Text>
                    </View>
                    
                </Card>
                <Card>
                    <CardSection>
                        <Button onPress={this.onAwesomeBookPress.bind(this)}>
                            Awesome Book
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onBookPress.bind(this)}>
                           Book
                        </Button>
                    </CardSection>
                    <CardSection>
                         <Button onPress={this.onBookPress.bind(this)}>
                            Book
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onBookPress.bind(this)}>
                            Book
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onBookPress.bind(this)}>
                            Book
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onBookPress.bind(this)}>
                            Book
                        </Button>
                    </CardSection>
                    <CardSection>
                       <Button onPress={this.onBookPress.bind(this)}>
                            Book
                        </Button>
                    </CardSection>
                </Card>
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    headingStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 30,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    }
};
const mapStateToProps = (state) => {
    const { data } = state.ChapterFetch;
    console.log(data.response);
    return { data };
};

export default connect(mapStateToProps,{chapterOneClick, chapter, fetchChapterOne })(HomePage);