import React from 'react';
import { Text, TouchableOpacity, Image } from 'react-native';

const Button = ({ onPress, children }) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Text style={textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 30,
    paddingTop: 10,
    paddingBottom: 10
  },
  buttonStyle: {
    justifyContent:'center',
    flexDirection: 'row',
    flex: 1,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#007aff',
    marginTop: '1%',
    marginLeft: '2%',
    marginRight: '2%',
    marginBottom: '2%'
  }
};

export { Button };
