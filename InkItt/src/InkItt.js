import React, {Component} from 'react';
import { createStore, applyMiddleware} from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
 
import RouterComponent from './RouterComponent'; 
import reducers from './reducers';

class InkItt extends Component {

    render(){
        const store = createStore(reducers, {},applyMiddleware(ReduxThunk));
        
        return(
            <Provider store={store}>
                <RouterComponent />
            </Provider>
        );
    }
}

export default InkItt;