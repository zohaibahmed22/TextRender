import {
    CHAPTERONE,
    CHAPTERFETCH
} from './Types';
import { Actions } from 'react-native-router-flux';


export const chapter = () => {
    return(dispatch) => {
        Actions.chapter();
    }
}
export const fetchChapterOne = () => {
    return(dispatch) => {
         fetch('https://cap_america.inkitt.de/1/stories/106766/chapters/1')
          .then((Response) => Response.json())
          .then((RespJSON) => {
              dispatch({ type: CHAPTERFETCH, payload: RespJSON});
        })
      };
};